
/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain;

/**
 * Entity class, model of entity domain, which identified by unique key.
 *
 * @author Lili
 * @param <KEY> the key of entity {@link  KEY}
 */
public interface Entity<KEY> {

    /**
     * Get entity key.
     *
     * @return key {@link KEY}
     */
    KEY getId();

}
