
/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain;

/**
 * Index class, domain of index model, which is identified by key{@link KEY} and
 * index {@link INDEX} .
 *
 * @author Lili
 * @param <KEY> the index key {@link  KEY}
 * @param <INDEX> the index value {@link INDEX}
 */
public interface Index<KEY, INDEX> extends Entity<KEY> {

    /**
     * Get index value {@link INDEX}.
     *
     * @return index value {@link INDEX}
     */
    INDEX getIndex();
}
