
/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain;

import java.util.UUID;

/**
 * UUID based entity, which the id will be created by its creation.
 *
 * @author lili
 */
public interface OperableEntity extends Entity<UUID> {

    /**
     * Set its id.
     *
     * @param id set entity id.
     */
    void setId(UUID id);
}
