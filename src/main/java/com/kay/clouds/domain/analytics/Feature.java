package com.kay.clouds.domain.analytics;

/**
 *
 * @author Lili
 */
public enum Feature {
    Profitability, Volume, Volatility, Beta
}
