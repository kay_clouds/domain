/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.exchange;

import com.kay.clouds.domain.Index;
import java.util.Date;

/**
 *
 * Domain description the exchange data.
 *
 * @author Lili
 */
public interface Exchange extends Index<String, Date> {

    @Override
    public default Date getIndex() {
        return getDate();
    }

    @Override
    public default String getId() {
        return getTicker();
    }

    /**
     *
     * @return the ticker (company.market).
     */
    String getTicker();

    /**
     *
     * @return the date of this exchange in format UTC.
     */
    Date getDate();

    /**
     *
     * @return the openning price of this exchange.
     */
    Double getOpenningPrice();

    /**
     *
     * @return the max price of this exchange in that day.
     */
    Double getDailyMaxPrice();

    /**
     *
     * @return the min price of this exchange in that day.
     */
    Double getDailyMinPrice();

    /**
     *
     * @return the closing price of this exchange in that day.
     */
    Double getClosingPrice();

    /**
     *
     * @return the exchange volume in that day.
     */
    Double getExchangeVolume();

    /**
     *
     * @return the exchange adjusted closing price in that day.
     */
    Double getAdjustedclosingPrice();

}
