/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.exchange;

import com.kay.clouds.domain.IndexRepository;
import java.util.Date;

/**
 * Data repository, to save and index data in to the data warehouse.
 *
 * @author Lili
 * @param <T> child class {@link T> of exchange {@link Exchange>
 */
public interface ExchangeRepository<T extends Exchange> extends IndexRepository<String, Date, T> {

}
