/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.exchange;

import java.util.List;

/**
 * Interface define data extraction service.
 *
 * @author Lili He
 */
public interface ExchangeService {

    /**
     * Scrapping by given ticker list.
     *
     * @param tickerList ticker list
     */
    void scrapping(List<String> tickerList);

}
