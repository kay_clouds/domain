/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.exchange;

import com.kay.clouds.domain.analytics.exchange.Exchange;
import com.kay.clouds.domain.analytics.exchange.ExchangeRepository;

/**
 * Index exchange repository, to save index exchange to the data warehouse.
 *
 * @author Lili
 */
public interface IndexExchangeRepository extends ExchangeRepository<Exchange> {

}
