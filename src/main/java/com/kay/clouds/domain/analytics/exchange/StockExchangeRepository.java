/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.exchange;

/**
 * Stock exchange repository, to save stock exchange to the data warehouse.
 *
 * @author Lili
 */
public interface StockExchangeRepository extends ExchangeRepository<Exchange> {

}
