/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.feature;

import com.kay.clouds.domain.analytics.Feature;
import java.util.Date;
import java.util.List;

/**
 *
 * Feature service, used to calculate stock feature by his history data.
 *
 * @author Lili
 */
public interface FeatureService {

    /**
     * Will calculate the list of result between the initial data and final
     * data.
     *
     * @param feature feature to calculate
     * @param tickerList stock ticker list
     * @param initialDate initial date of data
     * @param finalDate final date of data
     * @return list of number result {@link NumberResult}
     */
    List<NumberResult> calculate(
            Feature feature,
            List<String> tickerList,
            Date initialDate,
            Date finalDate
    );
}
