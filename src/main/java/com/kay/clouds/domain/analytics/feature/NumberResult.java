/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.feature;

import java.util.Date;
import java.util.List;

/**
 *
 * Feature result model.
 *
 * @author Lili
 * @param <T> Number type {@link T}.
 */
public interface NumberResult<T extends Number> {

    /**
     * Feature data model.
     *
     * @param <T> Number type {@link T}
     */
    public interface Data<T> {

        /**
         *
         * @return date of the data
         */
        Date getDate();

        /**
         *
         * @return the value {@link T} of data
         */
        T getValue();
    }

    /**
     *
     * @return stock ticker
     */
    String getTicker();

    /**
     *
     * @return list of data
     */
    List<Data<T>> getDataList();
}
