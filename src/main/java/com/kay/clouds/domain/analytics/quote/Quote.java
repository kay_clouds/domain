/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.quote;

import java.util.Date;

/**
 *
 * Domain description the quote data.
 *
 * @author Lili
 */
public interface Quote {

    /**
     *
     * @return the ticker (company.market).
     */
    String getTicker();

    /**
     *
     * @return the date of this exchange in format UTC.
     */
    Date getDate();

    /**
     *
     * @return previous closing price
     */
    Double getPreviousClosePrice();

    /**
     *
     * @return open price
     */
    Double getOpenPrice();

    /**
     *
     * @return bid price
     */
    Double getBidPrice();

    /**
     *
     * @return bid volume
     */
    Integer getBidVolume();

    /**
     *
     * @return ask price
     */
    Double getAskPrice();

    /**
     *
     * @return ask volume
     */
    Integer getAskVolume();

    /**
     *
     * @return volume
     */
    Integer getVolume();

    /**
     *
     * @return average volume
     */
    Integer getAverageVolume();

}
