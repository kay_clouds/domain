/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.quote;

/**
 * Interface that define the quote extraction service.
 *
 * @author Lili He
 */
public interface QuoteService {

    /**
     * Scrapping by given ticker.
     *
     * @param ticker ticker value
     */
    void scrapping(String ticker);

}
