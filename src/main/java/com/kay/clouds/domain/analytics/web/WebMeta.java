/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.web;

/**
 *
 * Domain describe the web meta data.
 *
 * @author daniel.eguia
 */
public interface WebMeta {

    /**
     *
     * @return name of page
     */
    String getName();

    /**
     *
     * @return description of page
     */
    String getDescription();

    /**
     *
     * @return Keywords of page
     */
    String getKeywords();

    /**
     *
     * @return image Url
     */
    String getImageUrl();

    /**
     *
     * @return page URL
     */
    String getPageUrl();

}
