/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.analytics.web;

/**
 * Interface that define the web meta data extraction service.
 *
 * @author daniel.eguia
 */
public interface WebMetaService {

    /**
     * Scrapping by given web URL.
     *
     * @param url to page
     * @return web meta data {@link WebMeta}
     */
    WebMeta scrapping(String url);

}
