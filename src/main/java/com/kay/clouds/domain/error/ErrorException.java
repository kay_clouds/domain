/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.error;

/**
 *
 * Error code based exception class.
 *
 * @author Lili
 */
public class ErrorException extends RuntimeException {

    /**
     * Error code, which caused exception.
     */
    private final ErrorCode error;

    public ErrorException(ErrorCode error) {
        super(error.get());
        this.error = error;
    }

    public ErrorCode getError() {
        return error;
    }

}
