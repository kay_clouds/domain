/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.counter;

import com.kay.clouds.domain.Counter;

/**
 *
 * Stock statistics.
 *
 * @author Lili
 */
public interface StockCounterSet {

    Counter getFollowCounter();

    Counter getLikeCounter();

}
