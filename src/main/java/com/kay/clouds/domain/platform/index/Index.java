/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.index;

import com.kay.clouds.domain.Entity;

/**
 *
 * Stock index entity model.
 *
 * @author Lili
 */
public interface Index extends Entity<String> {

    /**
     *
     * @return ticker as id.
     */
    @Override
    default String getId() {
        return getTicker();
    }

    /**
     *
     * @return index ticker.
     */
    String getTicker();

    /**
     *
     * @return name in English.
     */
    String getName();

    /**
     *
     * @return ISO 4217 currency code.
     */
    String getCurrencyCode();

    /**
     * Summary of index in English.
     *
     * @return description of the index.
     */
    String getDescription();

}
