/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.index.stock;

/**
 * Repository to manage index contains stock relation.
 *
 * @author Lili
 */
public interface IndexContainStockRepository extends IndexStockRepository {

}
