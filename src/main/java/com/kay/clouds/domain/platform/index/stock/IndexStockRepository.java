/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.index.stock;

import com.kay.clouds.domain.RelationRepository;
import com.kay.clouds.domain.platform.index.Index;
import com.kay.clouds.domain.platform.stock.Stock;

/**
 * Repository to manage relation from index to stock.
 *
 * @author Lili
 */
public interface IndexStockRepository
        extends RelationRepository<String, String, Index, Stock> {

}
