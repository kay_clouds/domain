/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.market;

import com.kay.clouds.domain.Entity;

/**
 * Market class, model of stock market domain.
 *
 * @author Lili
 */
public interface Market extends Entity<String> {

    /**
     *
     * @return Thomson Reuters Symbol as ID.
     */
    @Override
    String getId();

    /**
     *
     * @return name in English.
     */
    String getName();

    /**
     * ISO 3166-1 ALFA 3.
     *
     * @return its ISO country code
     */
    String getCountryCode();

    /**
     *
     * @return ISO 4217 currency code
     */
    String getCurrencyCode();

    /**
     * Summary of market in English.
     *
     * @return description of the market.
     */
    String getDescription();

    /**
     * Latitude of geography.
     *
     * @return latitude
     */
    Double getLatitude();

    /**
     * Longitude of geography.
     *
     * @return longitude
     */
    Double getLongitude();
}
