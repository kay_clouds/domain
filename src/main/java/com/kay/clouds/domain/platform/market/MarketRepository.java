/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.market;

import com.kay.clouds.domain.EntityRepository;

/**
 * Market repository.
 *
 * @author Lili
 */
public interface MarketRepository extends EntityRepository<String, Market> {

}
