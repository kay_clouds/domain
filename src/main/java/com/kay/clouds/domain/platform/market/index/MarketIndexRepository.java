/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.market.index;

import com.kay.clouds.domain.RelationRepository;
import com.kay.clouds.domain.platform.index.Index;
import com.kay.clouds.domain.platform.market.Market;

/**
 * Repository to manage relation from market to index.
 *
 * @author Lili
 */
public interface MarketIndexRepository
        extends RelationRepository<String, String, Market, Index> {

}
