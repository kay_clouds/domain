/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.stock;

import com.kay.clouds.domain.Entity;

/**
 * Stock domain.
 *
 * @author Lili
 */
public interface Stock extends Entity<String> {

    @Override
    default String getId() {
        return getTicker();
    }

    /**
     *
     * @return its ticker.
     */
    String getTicker();

    /**
     *
     * @return its sector.
     */
    String getSector();

    /**
     *
     * @return number of its share.
     */
    Long getShareNumber();

    /**
     *
     * @return its company id.
     */
    String getCompany();

    /**
     *
     * @return its industry
     */
    String getIndustry();

    /**
     * ISO 3166-1 ALFA 3.
     *
     * @return its ISO country code
     */
    String getCountryCode();

    /**
     *
     * @return its market (Thomson Reuters Symbol as ID).
     */
    String getMarket();

    /**
     *
     * @return its web company url.
     */
    String getCompanyUrl();

    /**
     *
     * @return ISO 4217 currency code.
     */
    String getCurrencyCode();

}
