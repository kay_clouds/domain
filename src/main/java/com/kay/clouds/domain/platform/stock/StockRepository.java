/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.stock;

import com.kay.clouds.domain.EntityRepository;

/**
 * Stock entity repository.
 *
 * @author Lili
 */
public interface StockRepository extends EntityRepository<String, Stock> {

}
