/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.user;

import com.kay.clouds.domain.OperableEntity;
import java.util.UUID;

/**
 *
 * User class, model of user domain.
 *
 * @author Lili
 */
public interface User extends OperableEntity {

    /**
     *
     * @return its id
     */
    @Override
    UUID getId();

    /**
     *
     * @return nickname.
     */
    String getNickName();

}
