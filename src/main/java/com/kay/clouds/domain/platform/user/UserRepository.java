/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.user;

import com.kay.clouds.domain.OperableEntityRepository;

/**
 *
 * User repository.
 *
 * @author Lili
 * @author daniel.eguia
 */
public interface UserRepository extends OperableEntityRepository<User> {

}
