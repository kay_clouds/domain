/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.user.stock;

/**
 *
 * User like stock repository.
 *
 * @author Lili
 * @author daniel.eguia
 */
public interface UserLikeStockRepository extends UserStockRepository {

}
