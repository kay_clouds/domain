/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.platform.user.stock;

import com.kay.clouds.domain.RelationRepository;
import com.kay.clouds.domain.platform.stock.Stock;
import com.kay.clouds.domain.platform.user.User;
import java.util.UUID;

/**
 *
 * User stock repository, manage relation form user to stock.
 *
 * @author daniel.eguia
 */
public interface UserStockRepository
        extends RelationRepository<UUID, String, User, Stock> {

}
