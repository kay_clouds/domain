
/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.security;

/**
 * Security permission list.
 *
 * @author Lili
 */
public enum Permission {
    /**
     * Access platform service.
     */
    PLATFORM,
    /**
     * Access analytics service.
     */
    ANALYTICS
}
