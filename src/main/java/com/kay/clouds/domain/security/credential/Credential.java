/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.security.credential;

import com.kay.clouds.domain.OperableEntity;
import com.kay.clouds.domain.security.Permission;
import java.util.List;
import java.util.UUID;

/**
 *
 * Define a credential for user or other subject.
 *
 * @author Lili
 */
public interface Credential extends OperableEntity {

    /**
     * Hashing a given password.
     *
     * @param passwordInText password in text
     * @return passwaod in hash
     */
    static String generatePassword(String passwordInText) {
        return UUID.nameUUIDFromBytes(passwordInText.getBytes()).toString();
    }

    /**
     *
     * @return id of credential
     */
    UUID getId();

    /**
     *
     * @return email of credential
     */
    String getEmail();

    /**
     *
     * @return password of credential
     */
    String getPassword();

    /**
     *
     * @return permission list of credential
     */
    List<Permission> getPermissionList();

}
