/*
 * Copyright (c) 2016, kay clouds and/or its affiliates. All rights reserved.
 *
 * KAY CLOUDS PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.kay.clouds.domain.security.credential;

import com.kay.clouds.domain.OperableEntityRepository;

/**
 * Credential repository to manager a credential.
 *
 * @author lili
 */
public interface CredentialRepository extends OperableEntityRepository<Credential> {

    /**
     *
     * @param email the registered email.
     * @return credential {@link Credential}
     */
    Credential findByEmail(String email);

}
